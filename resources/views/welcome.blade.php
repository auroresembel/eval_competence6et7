<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>eval_competence6-7</title>

        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}" defer></script>

        <!-- Fonts -->
        <link rel="dns-prefetch" href="//fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

         <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

        <!-- Fontawesome -->
        <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        

        <!-- Styles -->
        <link rel="stylesheet" type="text/css" href={{asset('css/app.css')}}>
    </head>
    <body>
    <div class="jumbotron">
        <i class="fas fa-map-signs"></i>
        <h1 class="display-4">Bienvenue!</h1>
            <p class="lead">Ce site vous permet de savoir la distance entre vous et une autre ville.</p>
    </div>
            
        <!-- ajouter un lieux  -->
            <div class="creat">
                <p>Souhaitez-vous ajouter une lieux?</p>
                <button type="button" class="btn btn-outline-info" data-toggle="modal" data-target="#Ajouter">Créer</button>
                
                <form method="post" action="{{route ('lieuxs.store')}}" enctype="multipart/form-data">
                    @csrf
                    <div class="modal fade" id="Ajouter" tabindex="-1" role="dialog" aria-labelledby="ajouter" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="modifier_MDPLabel">Ajouter un lieux</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div>
                                        <p>Titre </p>
                                        <input type="text" name="Titre">
                                        <p>Adresse</p>
                                        <input type="text" name="Adresse"> 
                                        <p>Code Potal </p>
                                        <input type="text"name="CodePostal">
                                        <p>Ville</p>
                                        <input type="text"name="Ville">
                                        <p>Latitude</p>
                                        <input type="text" name="Latitude">
                                        <p>Longitude</p>
                                        <input type="text" name="Longitude">
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Annuler</button>
                                        <button type="submit" class="btn btn-outline-success">Enregistrer</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div> 

    <!-- afficher les cartes -->
            </div class="cards">
                @foreach ($lieuxs as $lieu)
                    <div class="card text-center" style="width: 22rem;">
                        <div class="card-body">
                            <h5 class="card-title ">{{ $lieu->Titre}}</h5>
                            <p class="card-text">{{ $lieu->Adresse}}<br>
                                {{ $lieu->CodePostal}} {{ $lieu->Ville}}<br>
                                {{ $lieu->Latitude}}<br>
                                {{ $lieu->Longitude}}</p>
                            <button type="button" class="btn btn-secondary" >Calculer la distance</button><br>
                            <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#modifier{{$lieu->id}}">Modifier</button>
                            <button type="button" class="btn btn-danger"data-toggle="modal" data-target="#supprimer{{$lieu->id}}">Supprimer</button>
                        </div>
                    </div>
                    
                    
    <!-- mofier une carte -->
            <form method="POST" action="{{route ('lieuxs.update', $lieu->id)}}" enctype="multipart/form-data">
            @method('PUT') 
            @csrf
                <div class="modal fade" id="modifier{{$lieu->id}}" tabindex="-1" role="dialog" aria-labelledby="modifier_MDPLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="modifier_MDPLabel">Modifier les éléments d'un lieux</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div>
                                    <p>Titre </p>
                                    <input type="text" class="Titre" name="Titre">
                                    <p>Adresse</p>
                                    <input type="text" class="Adresse" name="Adresse"> 
                                    <p>Code Potal </p>
                                    <input type="text" class="CodePostal" name="CodePostal">
                                    <p>Ville</p>
                                    <input type="text" class="Ville" name="Ville">
                                    <p>Latitude</p>
                                    <input type="text" class="Latitude" name="Latitude">
                                    <p>Longitude</p>
                                    <input type="text" class="Longitude" name="Longitude">
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Annuler</button>
                                    <button type="submit" class="btn btn-outline-success">Enregistrer</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <form >
            

<!-- supprimer un lieux -->
            <form action="{{ route('lieuxs.destroy', $lieu->id)}}" method="post">
                  @csrf
                  @method('DELETE')
                <div class="modal fade" id="supprimer{{$lieu->id}}" tabindex="-1" role="dialog" aria-labelledby="supprimer" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div>
                                    <p>Etes vous sùr de vouloir supprimer ce lieux ?</p>
                             
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Annuler</button>
                                    <button type="submit" class="btn btn-outline-success">Valider</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </form>
            </div>
                @endforeach
            </div>

     
        

        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    </body>
    
</html>
