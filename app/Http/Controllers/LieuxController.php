<?php

namespace App\Http\Controllers;

use App\Lieux;
use Illuminate\Http\Request;

class LieuxController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
 
        $lieuxs = Lieux::all();
 
        return view('welcome', compact('lieuxs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('lieuxs.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $lieux = new Lieux([
            'Titre' => $request->get('Titre'),
            'Adresse' => $request->get('Adresse'),
            'CodePostal' => $request->get('CodePostal'),
            'Ville' => $request->get('Ville'),
            'Latitude' => $request->get('Latitude'),
            'Longitude' => $request->get('Longitude')
        ]);
        $lieux->save();
        return redirect('/')->with('success');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Lieux  $lieux
     * @return \Illuminate\Http\Response
     */
    public function show(Lieux $lieux)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Lieux  $lieux
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $lieux = Lieux::find($id);
 
        return view('welcome', compact('lieux'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Lieux  $lieux
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $lieux = Lieux::find($id);
        $lieux->Titre =  $request->get('Titre');
        $lieux->Adresse = $request->get('Adresse');
        $lieux->CodePostal = $request->get('CodePostal');
        $lieux->Ville = $request->get('Ville');
        $lieux->Latitude = $request->get('Latitude');
        $lieux->Longitude = $request->get('Longitude');
        $lieux->save();

        return redirect('/')->with('success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Lieux  $lieux
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $lieux =Lieux::find($id);
        $lieux->delete();
   
        return redirect('/')->with('success');
    }
}
