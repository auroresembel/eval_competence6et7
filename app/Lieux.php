<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lieux extends Model
{
    protected $fillable = [
        'id',
        'Titre',
        'Adresse',
        'CodePostal',
        'Ville',
        'Latitude',
        'Longitude'
    ];
}
